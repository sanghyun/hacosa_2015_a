/* jshint node:true, -W079 */

'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var del = require('del');
var runSequence = require('run-sequence');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
// var ssi = require('browsersync-ssi');
//var include = require('gulp-lb-include');
var autoprefixer = require('gulp-autoprefixer');
var path = require('path');



// =============================================================================
// 경로
// =============================================================================

var src = {
	css: [
		'**/css/_mgm/*.css',
		'!**/css/_mgm/*.min.css',
		'!node_modules/'
	],
	js: [
		'**/js/_mgm/**/*.js',
		'!**/js/_mgm/**/*.min.js',
		'!node_modules/'
	]
};
var dest = {
	maps: '.'
};

// =============================================================================
// 파일 생성 경로 얻기
// =============================================================================

function getDest(file) {
	var newDirName = 'mgm',
	//newDirName = path.extname(file.path).replace('.', ''),
		srcDir,
		destDir;
	
	if (newDirName !== 'map') {
		console.log(path.basename(file.path));
		if (newDirName === 'json') {
			newDirName = 'mgm';
		}
		srcDir = path.sep + '_' + newDirName + path.sep;
		destDir = path.sep + newDirName + path.sep;
		
		file.path = file.path.replace(srcDir, destDir);
	}

	return file.base;
}

// =============================================================================
// .map 파일 삭제
// =============================================================================

gulp.task('clean', function (cb) {
	$.cached.caches = {};

	del([
		'**/*.map'
	], cb);
});



// =============================================================================
// JS
// =============================================================================

/*
sequences     : true,  // join consecutive statemets with the “comma operator”
properties    : true,  // optimize property access: a["foo"] → a.foo
dead_code     : true,  // discard unreachable code
drop_debugger : true,  // discard “debugger” statements
unsafe        : false, // some unsafe optimizations (see below)
conditionals  : true,  // optimize if-s and conditional expressions
comparisons   : true,  // optimize comparisons
evaluate      : true,  // evaluate constant expressions
booleans      : true,  // optimize boolean expressions
loops         : true,  // optimize loops
unused        : true,  // drop unused variables/functions
hoist_funs    : true,  // hoist function declarations
hoist_vars    : false, // hoist variable declarations
if_return     : true,  // optimize if-s followed by return/continue
join_vars     : true,  // join var declarations
cascade       : true,  // try to cascade `right` into `left` in sequences
side_effects  : true,  // drop side-effect-free statements
warnings      : true,  // warn about potentially dangerous optimizations/code
global_defs   : {}     // global definitions
*/

var uglifyOptions = {
	preserveComments: 'some'
};

gulp.task('js', function () {
	return gulp.src(src.js)
		.pipe($.plumber())
		.pipe($.cached('js'))
		.pipe($.sourcemaps.init())
			.pipe($.uglify(uglifyOptions))
		.pipe($.sourcemaps.write(dest.maps))
		.pipe(gulp.dest(getDest));
});

// =============================================================================
// Copy
// =============================================================================

gulp.task('copy', function () {
	return gulp.src([
			'**/*.min.*',
			'**/*.json',
			'!package.json',
			'!**/css/mgm/**/*.min.*',
			'!**/js/mgm/**/*.min.*',
			'!node_modules/**/*'
		])
		.pipe($.cached('copy'))
		.pipe(gulp.dest(getDest));
});

// =============================================================================
// include build
// =============================================================================
gulp.task('include', function () {
	return gulp.src('html_origin/**/*.html')
		.pipe($.plumber())
		.pipe($.lbInclude())
		.pipe($.filter([
			'**/*.html',
			'!include/**/*.html',
			'!component/**/*.html'
		]))
		.pipe(gulp.dest('html/'));
});


// =============================================================================
// CSS
// =============================================================================

var minifyCssOptions = {
	advanced: false,
	aggressiveMerging: false,
	shorthandCompacting: false
};

// 기존에 생성된 파일 삭제
gulp.task('clean', function (cb) {
  del([
      'resources/css/mgm/'
    ], cb);
});

// CSS
gulp.task('less', function () {
	return gulp.src(['resources/css/_mgm_less/**/*.less', '!resources/css/_mgm_less/**/_*.less'])
		.pipe($.plumber())
			.pipe($.less())
			.pipe(autoprefixer({
	            browsers: ['> 1%','last 2 versions','android > 2.3'],
	            cascade: false
	        }))
			.pipe(gulp.dest('resources/css/_mgm/'))
		.pipe($.sourcemaps.init())
			.pipe($.minifyCss(minifyCssOptions))
		.pipe($.sourcemaps.write(dest.maps))
		.pipe(gulp.dest(getDest))
		.pipe($.if(browserSync.active, reload({stream: true})));
});
gulp.task('maps', function (cb) {
	runSequence('clean', ['js', 'copy'], cb);
});
// Server
gulp.task('serve', ['maps','less','include'], function () {
	browserSync({
		server: {
			baseDir: './',
			directory: true,
			codeSync: false,
			injectChanges: false,
			ghostMode: false,
		},
	    watchOptions: {
	      interval: 500
	    }
	});


	gulp.watch('html_origin/**/*.html', { interval: 500 }, ['include']);
	gulp.watch('resources/css/_mgm_less/**/*.less', { interval: 500 }, ['less']);
	gulp.watch(src.js, { interval: 500 }, ['js']);


});

// Default
gulp.task('default', function (cb) {
	runSequence('clean', ['serve', 'js', 'copy'], cb);
});


//gulp.task('default', ['watch']);


